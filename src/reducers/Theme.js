import { COLORS } from '../themes';

const initialState ={
    colorData: COLORS.red,
    isAuthenticated: false,
    signed: false 
};

const Theme =(state = initialState, action) => {
    switch(action.type) {
        case 'TOGGLE_THEME':
            switch(action.payload.name) {
                case 'red':
                    return {colorData: COLORS.blue};
                case 'blue':
                    return {colorData: COLORS.red};
            }
            break;
            case "SIGNUP":
                //newState.age += action.value;
                    return{
                    ...state,
                    signed: action.value,
                    }
            break;
            default:
                return state;
    }
}

export default Theme;