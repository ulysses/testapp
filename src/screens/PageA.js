import React, { Component } from 'react';
import { View, Text, TextInput, Button, Alert } from 'react-native';

import ToggleTheme from '../components/ToggleTheme';
import ApiKeys from '../constants/ApiKeys';
import * as firebase from 'firebase';

export default class PageA extends Component {

  state = { email: '', password: '', errorMessage: null }


  onSignup =() =>{
    firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.password)
      .then(()=> {
          Alert.alert('you signed up!')
          this.setState({ email:'', password:'' })
      }, (error)=> {
          Alert.alert(error.message)
      });

  }

  onSignin =() => {
    firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password)
      .then(()=>{
          Alert.alert('you are signed in')
          this.setState({ isAuthenticated: true, email:'', password:'' })
      },(error) =>{
          Alert.alert(error.message)
      })
}

  componentDidMount () {
    if (!firebase.apps.length) { firebase.initializeApp(ApiKeys.FirebaseConfig); }
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <ToggleTheme />
        <View style={{paddingTop: 20}}>
                <View style ={{paddingTop:50, alignItems:"center"}}>
        
                  <View>
                      <Text>Email</Text>
                    <TextInput style={{width: 200, height: 40, borderWidth: 1}}
                      value ={this.state.email}
                      onChangeText={(text) => {this.setState({ email : text})}} 
                      />
                </View>

                <View>
                <Text>password</Text>
                    <TextInput style={{width: 200, height: 40, borderWidth: 1}}
                      value ={this.state.password}
                      onChangeText={(text) => {this.setState({ password : text})}} 
                      />
                </View>
            
            <Button title = 'signup' onPress={this.onSignup}  />
            <Button title = 'login'  onPress={this.onSignin}/>    
        </View>
        </View>
      </View>
    );
  }
}